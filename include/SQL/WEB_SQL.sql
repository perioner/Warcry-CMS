/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50137
Source Host           : localhost:3306
Source Database       : web2

Target Server Type    : MYSQL
Target Server Version : 50137
File Encoding         : 65001

Date: 2010-03-07 10:09:55
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `accounts_more`
-- ----------------------------
DROP TABLE IF EXISTS `accounts_more`;
CREATE TABLE `accounts_more` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `acc_login` varchar(55) COLLATE latin1_general_ci NOT NULL,
  `vp` bigint(55) NOT NULL DEFAULT '0',
  `question_id` int(11) NOT NULL,
  `answer` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `dp` bigint(55) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`acc_login`)
) ENGINE=MyISAM AUTO_INCREMENT=104 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of accounts_more
-- ----------------------------

-- ----------------------------
-- Table structure for `comments`
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `poster` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `content` text COLLATE latin1_general_ci NOT NULL,
  `newsid` int(11) NOT NULL,
  `timepost` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `datepost` varchar(100) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=145 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of comments
-- ----------------------------

-- ----------------------------
-- Table structure for `donations`
-- ----------------------------
DROP TABLE IF EXISTS `donations`;
CREATE TABLE `donations` (
  `invoice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `receiver_email` varchar(60) COLLATE latin1_general_ci DEFAULT NULL,
  `item_name` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `item_number` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `quantity` varchar(6) COLLATE latin1_general_ci DEFAULT NULL,
  `payment_status` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `pending_reason` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `payment_date` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `mc_gross` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `mc_fee` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `tax` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `mc_currency` varchar(3) COLLATE latin1_general_ci DEFAULT NULL,
  `txn_id` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `txn_type` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `first_name` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `last_name` varchar(40) COLLATE latin1_general_ci DEFAULT NULL,
  `address_street` varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  `address_city` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `address_state` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `address_zip` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `address_country` varchar(30) COLLATE latin1_general_ci DEFAULT NULL,
  `address_status` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `payer_email` varchar(60) COLLATE latin1_general_ci DEFAULT NULL,
  `payer_status` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `payment_type` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `notify_version` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `verify_sign` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `referrer_id` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`invoice`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of donations
-- ----------------------------

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `content` longtext COLLATE latin1_general_ci NOT NULL,
  `iconid` int(11) NOT NULL,
  `timepost` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `datepost` varchar(100) COLLATE latin1_general_ci NOT NULL,
  `author` varchar(50) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('72', 'Welcome', '<p><br><center>Thank you for using WebWoW Creator!<br><a href=\"http://www.web-wow.net\" target=\"_blank\">www.web-wow.net</a></center><br><br><br></p>', '0', '1258378286', 'November 16, 2009', 'Admin');

-- ----------------------------
-- Table structure for `pages`
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `link` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `description` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1' COMMENT '1 for default 2 for below',
  `orderby` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of pages
-- ----------------------------
INSERT INTO `pages` VALUES ('64', 'Server Status', './quest.php?name=status', 'Check if server is online or offline, and view stats.xml.', '1', '1');
INSERT INTO `pages` VALUES ('65', 'Honor Statistics', './honor.php', '', '1', '2');
INSERT INTO `pages` VALUES ('66', 'Top Kills Statistics', './hk.php', '', '1', '1');
INSERT INTO `pages` VALUES ('69', 'Downloads', './quest.php?name=downloads', '', '2', '1');

-- ----------------------------
-- Table structure for `paypal_data`
-- ----------------------------
DROP TABLE IF EXISTS `paypal_data`;
CREATE TABLE `paypal_data` (
  `id` bigint(21) NOT NULL AUTO_INCREMENT,
  `login` varchar(55) COLLATE latin1_general_ci NOT NULL,
  `txnid` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `amount` bigint(21) NOT NULL,
  `who` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `whendon` bigint(100) NOT NULL DEFAULT '0',
  `comment` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=126 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of paypal_data
-- ----------------------------

-- ----------------------------
-- Table structure for `shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sep` varchar(3) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `name` text COLLATE latin1_general_ci NOT NULL,
  `itemid` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `color` tinytext COLLATE latin1_general_ci NOT NULL,
  `cat` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `sort` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `cost` varchar(11) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `charges` varchar(11) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `donateorvote` int(5) NOT NULL DEFAULT '0' COMMENT '0 is vote 1 is donation item',
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT 'No Description',
  `custom` varchar(3) COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of shop
-- ----------------------------
INSERT INTO `shop` VALUES ('40', '0', 'Robes of Arugal', '6324', '3', 'armor', '1', '1', '1', '0', 'Nice looking robes for 20ish levels.', '0');

-- ----------------------------
-- Table structure for `vote_data`
-- ----------------------------
DROP TABLE IF EXISTS `vote_data`;
CREATE TABLE `vote_data` (
  `id` bigint(21) NOT NULL AUTO_INCREMENT,
  `userid` bigint(21) NOT NULL,
  `siteid` bigint(21) NOT NULL,
  `timevoted` bigint(21) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=143 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- ----------------------------
-- Records of vote_data
-- ----------------------------
